# Simplified E-mails

This addon added a MAIL template override to use a very reduced e-mail template with no logo.

This was removed as of Composr v11 beta7. With the introduction of business address (soon), un-subscribe link, and other CAN-SPAM requirements, we cannot effectively reduce mail much without removing elements required by CAN-SPAM policies. Furthermore, this addon interfered with HTML / plain-text mail consistency.
