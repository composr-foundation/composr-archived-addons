# Composr Archived Addons

## Introduction
This repository contains addons which have been archived from Composr CMS (removed from the main codebase).

There are several reasons an addon may be removed. These may include the following:
- Low interest in the use and/or maintenance of the addon
- Addon utilises third-party code which is no longer being maintained
- Addon is replaced by another addon
- Addon contains functionality which is no longer compatible with the latest versions of Composr CMS / PHP / SQL. And it is impractical to try and make it compatible.
- Addon is no longer in line with the core vision of Composr CMS

## Addons in this repository
The following addons are currently in this repository:
- bantr
- confluence
- demonstratr
- performance_compile
- simplified_emails
- sugarcrm
- trickstr
- twitter_support

Please see https://compo.sr/tracker/view.php?id=5687 for a list of addons currently nominated for archiving and to comment on their nomination.

## Limitations of liability
We grant no warranty on the code in this repository. We are not liable for anything that may happen resulting from attempting to use these old addons. They will likely no longer work in Composr CMS and are only provided here for reference or in the event someone would like to maintain the addon or hire a developer to maintain and implement it.

## Licence
Please see the respective sources[_custom]/hooks/systems/addon_registry file, or the addon.inf file, for the licence and copyright of each addon.