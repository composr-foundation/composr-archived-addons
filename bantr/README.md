# Bantr

This addon was a fun / silly one which randomly sent an insulting Private Topic to a member, and if they responded correctly (Monkey Island style), they got points.

This addon was archived because it serves no functional purpose for Composr and is more annoying than anything (99.9% of people won't know what to respond unless they knew the code base).