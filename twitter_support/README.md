# Twitter Support

This addon enabled the capability of setting up Twitter feeds and easy log-in via Twitter.

This addon was removed / archived as of version 11.beta1 due to Twitter being bought out by Elon Musk, changed to X, and the APIs no longer being free.