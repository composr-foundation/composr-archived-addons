# Performance Compile

This addon allowed webmasters to compile overridden code together for performance.

This addon was removed / archived as of version 11.beta7 for several reasons:
 - It does not support contentious overrides
 - Composr's underlying compilation system has changed, and code overrides now automatically compile into _compiled files at runtime when needed (including contentious overrides). Therefore, the addon is virtually useless now.
 