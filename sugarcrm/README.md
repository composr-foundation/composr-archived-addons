# SugarCRM

This addon enabled the ability to sync users and leads with the SugarCRM (community edition) software.

This addon was archived because SugarCRM is not Open Source anymore and there is no one to maintain or sponsor this addon.