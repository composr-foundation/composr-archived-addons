# Trickstr

This addon added the capability of implementing chat bots in Composr's chat rooms.

This addon was removed / archived as of version 11.beta1 because programe is not PHP 8 compatible. Furthermore, Trickstr has been a source of several bugs over the v11 development. And with the rise of AI, we found chat bots of this nature to be irrelevant.