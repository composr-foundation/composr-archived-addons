# Demonstratr

This addon enabled the capability of setting up instant personal demos of the software on a site or setting up shared installs under a main one.

This addon was removed / archived as of version 11.alpha3 due to the high maintenance / security implications, the low usability, and the fact almost all CMS defer personal demos to other services like Softaculous.

This was originally part of the composr_homesite addon before it was refactored and archived.