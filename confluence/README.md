# Confluence

This addon enabled the ability to run documentation natively within Composr from a self-hosted instance of Atlassian's Confluence software.

This addon was archived due to Atlassian no-longer supporting self-hosted instances of Confluence, and the Cloud version being significantly different. Additionally, no clients were using Confluence anymore. Therefore, we had no reason to continue maintaining it.